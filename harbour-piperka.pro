# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-piperka

CONFIG += sailfishapp

SOURCES += src/harbour-piperka.cpp \
    src/comic.cpp \
    src/download.cpp \
    src/user.cpp \
    src/subscription.cpp \
    src/updates.cpp \
    src/browse.cpp \
    src/application.cpp \
    src/page.cpp \
    src/sortmanager.cpp \
    src/passwordvalidator.cpp

DISTFILES += qml/harbour-piperka.qml \
    qml/cover/CoverPage.qml \
    rpm/harbour-piperka.changes.in \
    rpm/harbour-piperka.changes.run.in \
    rpm/harbour-piperka.spec \
    rpm/harbour-piperka.yaml \
    translations/*.ts \
    harbour-piperka.desktop \
    qml/pages/BrowsePage.qml \
    qml/pages/MainPage.qml \
    qml/pages/LoginPage.qml \
    qml/pages/ReaderPage.qml \
    qml/pages/PageDetailPage.qml \
    qml/pages/UpdatesPage.qml \
    qml/pages/AllReadPage.qml \
    qml/pages/InitialLoadingPage.qml \
    qml/pages/NewAccountPage.qml \
    qml/pages/NetworkErrorPage.qml \
    qml/pages/ForceLogout.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

TRANSLATIONS += translations/harbour-piperka-fi.ts

HEADERS += \
    src/comic.h \
    src/download.h \
    src/user.h \
    src/subscription.h \
    src/updates.h \
    src/browse.h \
    src/application.h \
    src/page.h \
    src/sortmanager.h \
    src/passwordvalidator.h

DEFINES += APP_VERSION=\\\"$$VERSION\\\"
