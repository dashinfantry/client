/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

#pragma once

#include <QAbstractListModel>
#include <QPointer>

#include "subscription.h"
#include "updates.h"
#include "user.h"

class Page
{
public:
    Page(const int &ord, const QString &name, const int subPages);

    int ord() const { return m_ord; }
    int subPages() const { return m_subPages; }
    const QString &name() const { return m_name; }

private:
    int m_ord;
    QString m_name;
    int m_subPages;
};

class PageModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(QModelIndex cursor READ cursor NOTIFY cursorChanged)
    Q_PROPERTY(QModelIndex subscription READ subscription NOTIFY subscriptionChanged)
    Q_PROPERTY(QString cursorUri READ cursorUri NOTIFY cursorChanged)
    Q_PROPERTY(int rowCount READ rowCount NOTIFY rowCountChanged)
    Q_PROPERTY(QString homepage READ homepage NOTIFY pagesLoaded)

    // True if navigating forward updates the user's book mark
    Q_PROPERTY(bool autoBookmark READ autoBookmark WRITE setAutoBookmark NOTIFY autoBookmarkChanged)
    // True if navigating forward from last page takes to another comic with unread pages
    Q_PROPERTY(bool autoSwitch READ autoSwitch WRITE setAutoSwitch)
    // Is forward navigation button enabled
    Q_PROPERTY(bool haveNext READ haveNext NOTIFY haveNextChanged)
    // Is forward navigation a skip to next unread comic
    Q_PROPERTY(bool nextIsSwitch READ nextIsSwitch NOTIFY nextIsSwitchChanged)
    // Has the user read everything and navigated forward from the last unread page
    Q_PROPERTY(bool allRead READ allRead NOTIFY allReadChanged)

public:
    enum PageRoles {
        OrdRole = Qt::UserRole + 1
        ,NameRole
        ,UriRole
        ,CursorRole
        ,CurrentMarkerRole
        ,SubscribedRole
    };

    PageModel(UpdatesModel &updates, User &user, QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;

    Q_INVOKABLE void loadComic(int cid);
    Q_INVOKABLE int cid() const {return m_cid;}
    Q_INVOKABLE void setCursor(int ord);
    Q_INVOKABLE void setCursorNext();
    QModelIndex cursor() const;
    QModelIndex subscription() const;
    Q_INVOKABLE bool invalidSubscription() const;

    QString cursorUri() const;
    QString homepage() const { return m_homepage; }
    bool autoBookmark() const { return m_autoBookmark; }
    void setAutoBookmark(const bool &bm);
    bool autoSwitch() const { return m_autoSwitch; }
    void setAutoSwitch(const bool &as) { m_autoSwitch = as; }
    bool haveNext() const { return m_haveNext; }
    bool nextIsSwitch() const {return m_nextIsSwitch;}
    Q_INVOKABLE bool switchNext();
    bool allRead() const {return m_allRead; }

signals:
    void loadPages(int cid);
    void loadingChanged();
    void cursorChanged();
    void subscriptionChanged();
    void rowCountChanged();
    void pagesLoaded();
    void autoBookmarkChanged();
    void haveNextChanged();
    void nextIsSwitchChanged();
    void allReadChanged();

public slots:
    void loadPagesComplete(const QJsonDocument &doc);
    void setSubscription(const QPointer<Subscription> &subs);

private slots:
    void subscriptionMoved(const int &old);
    void unsubscribing();

protected:
    QHash<int, QByteArray> roleNames() const;

private:
    UpdatesModel &m_updates;
    // Used to invoke subscriptions on autoBookmark mode
    User &m_user;

    int m_cid = -1;
    int m_cursor = 0;
    QList<Page> m_pages;
    QString m_url_base;
    QString m_url_tail;
    QString m_fixed_head;
    QString m_homepage;
    QPointer<Subscription> m_sub;
    bool m_autoBookmark = false;
    bool m_haveNext = false;
    bool m_nextIsSwitch = false;
    bool m_autoSwitch;
    bool m_allRead = false;
};
