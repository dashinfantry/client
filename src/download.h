/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

#pragma once

#ifdef PIPERKA_DEVEL
#define PIPERKA_HOST "dev.piperka.net"
#else
#define PIPERKA_HOST "piperka.net"
#endif

#include <QNetworkAccessManager>
#include <QObject>
#include <QUrl>

class Download : public QObject
{
    Q_OBJECT

public:
    explicit Download(QObject *parent = 0);

    // These functions are coupled to User
    QNetworkReply *createAccount(const QString &user, const QString &email, const QString &password,
                                 const QMap<int, int> &bookmarks);
    QNetworkReply *login(const QString &user, const QString &password,
                         const QMap<int, int> *bookmarks);
    QNetworkReply *logout(const QString &token);
    QNetworkReply *subscribe(const QString &token, const int &cid, const int &ord);
    QNetworkReply *unsubscribe(const QString &token, const int &cid);
    QNetworkReply *bookmarkPositions(const QMap<int, int> &bm);
    void forgetUser();

    QNetworkReply *userPrefs();

    void setSession(const QByteArray &session);

signals:
    // Error handled as syncError
    void downloadComicsListComplete(const QJsonDocument &doc);
    void downloadComicsListUnchanged();

    void downloadPagesComplete(const QJsonDocument &doc);
    void downloadPagesError(QNetworkReply *reply);

    void downloadSortComplete(int sortType, const QJsonDocument &doc);
    void downloadSortError(QNetworkReply *reply);

    void downloadComicsListError(QNetworkReply *reply);
public slots:
    void downloadComicsList();
    void downloadSort(int sortType);
    void downloadPages(int cid);

private slots:
#ifdef PIPERKA_DEVEL
    void ignoreSSL(QNetworkReply *reply, const QList<QSslError> &errors);
#endif

private:
    QNetworkAccessManager mgr;
    QByteArray comicsListStamp;

    QUrl uri_comicsOrdered = QUrl("https://" PIPERKA_HOST "/d/comics_ordered.json");
    QUrl uri_createAccount = QUrl("https://" PIPERKA_HOST "/s/createAccount");
    QUrl uri_login = QUrl("https://" PIPERKA_HOST "/s/login");
    QUrl uri_logout = QUrl("https://" PIPERKA_HOST "/");
    QUrl uri_userPrefs = QUrl("https://" PIPERKA_HOST "/s/uprefs");
    QUrl uri_sort = QUrl("https://" PIPERKA_HOST "/s/sortedlist");
    QUrl uri_archive = QUrl("https://" PIPERKA_HOST "/s/archive/");
    QUrl uri_bookmarkPositions = QUrl("https://" PIPERKA_HOST "/s/lookupPositions");
};
