/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

#pragma once

#include <QHash>
#include <QJsonDocument>
#include <QSortFilterProxyModel>
#include <QTimer>

#include "comic.h"
#include "sortmanager.h"
#include "subscription.h"

class UpdatesModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int sortType READ sortType WRITE setSortType NOTIFY sortTypeChanged)
    Q_PROPERTY(int unreadPages READ unreadPages NOTIFY unreadPagesChanged)
    Q_PROPERTY(bool noUnread READ noUnread NOTIFY noUnreadChanged)
    Q_PROPERTY(bool subscriptionFlag READ subscriptionFlag NOTIFY subscriptionFlagChange)
public:
    UpdatesModel(const SortManager &mgr, QObject *parent = 0);

    void setSource(QAbstractItemModel *model);

    int sortType() const { return m_sortType; }
    void setSortType(const int &sortType);
    int unreadPages() const { return m_unreadCount;}
    bool noUnread() const { return m_noUnread; }
    bool subscriptionFlag() const { return m_subscriptionFlag; }

    Q_INVOKABLE int firstCid() const;

signals:
    void sortTypeChangePrepare(int sortType);
    void sortTypeChanged();
    void unreadPagesChanged();
    void noUnreadChanged();
    void subscriptionFlagChange();

public slots:
    void sortDownloaded(const int &sortType, const QJsonDocument &doc);
    void subscriptionChange();

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
    bool filterAcceptsColumn(int source_column, const QModelIndex &source_parent) const override {
        Q_UNUSED(source_column);
        Q_UNUSED(source_parent);
        return true;
    }
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;

private:
    int m_sortType = 0;
    int m_unreadCount = 0;
    bool m_noUnread = true;
    const SortManager &m_mgr;
    bool m_subscriptionFlag = false;
    QTimer subscriptionTimeout;

    int countUnreadPages() const;
};
