/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: detailPage
    allowedOrientations: Orientation.All

    SilicaListView {
        id: pageList
        model: pageModel
        anchors.fill: parent
        currentIndex: -1

        VerticalScrollDecorator {
            id: verticalDecorator
            flickable: pageList
        }

        header: Column {
            id: content

            width: parent.width
            PageHeader {
                title: qsTr("Details")
            }

            Item {
                width: 1
                height: Theme.paddingMedium
            }

            LinkedLabel {
                plainText: qsTr("Use the following link to open in external browser")+"\n" + pageModel.cursorUri
                anchors.leftMargin: Theme.paddingSmall
                width: parent.width
                onLinkActivated: Qt.openUrlExternally(link)
            }

            Item {
                width: 1
                height: Theme.paddingMedium
            }

            Item {
                height: Theme.itemSizeMedium
                width: parent.width

                Button {
                    id: inBrowser
                    text: qsTr("Copy to clipboard")
                    anchors.left: parent.left
                    width: (parent.width-Theme.horizontalPageMargin)/2
                    onClicked: Clipboard.text = pageModel.cursorUri
                }
                Button {
                    text: qsTr("Homepage")
                    anchors.right: parent.right
                    width: (parent.width-Theme.horizontalPageMargin)/2
                    onClicked: Qt.openUrlExternally(pageModel.homepage)
                }
            }

            SectionHeader {
                text: qsTr("Jump to...")
            }

            Item {
                height: Theme.itemSizeMedium
                width: parent.width

                Button {
                    id: focusCursor
                    text: qsTr("Currently viewing")
                    anchors.left: parent.left
                    width: (parent.width-Theme.horizontalPageMargin)/2
                    onClicked: {
                        pageList.currentIndex = -1
                        pageList.currentIndex = pageModel.cursor.row
                        verticalDecorator.showDecorator()
                    }
                }

                Button {
                    text: qsTr("Subscribed")
                    anchors.right: parent.right
                    width: (parent.width-Theme.horizontalPageMargin)/2
                    enabled: pageModel.subscription.valid
                    onClicked: {
                        pageList.currentIndex = pageModel.subscription.row
                        verticalDecorator.showDecorator()
                    }
                }
            }

            SectionHeader {
                text: qsTr("Subscription controls")
                visible: user.logged
            }

            TextSwitch {
                visible: user.logged
                text: qsTr("Move bookmark on navigation")
                checked: pageModel.autoBookmark
                onClicked: pageModel.autoBookmark = checked
            }

            Item {
                width: 1
                height: Theme.paddingMedium
            }
        }


        delegate: ListItem {
            id: delegate

            Row {
                x: Theme.horizontalPageMargin
                height: Theme.itemSizeSmall
                width: parent.width

                Label {
                    width: Theme.itemSizeSmall
                    text: 1+ord
                    font.bold: cursor
                    anchors.verticalCenter: parent.verticalCenter
                    visible: !currentMarker
                }

                Image {
                    source: "image://theme/icon-s-favorite"
                    visible: subscribed
                    anchors.verticalCenter: parent.verticalCenter
                }

                Label {
                    text: name ? name : "sadf"
                    font.bold: cursor
                    anchors.verticalCenter: parent.verticalCenter
                    visible: !currentMarker && name !== undefined
                }

                Label {
                    text: qsTr("Newest page")
                    font.italic: true
                    font.bold: cursor
                    anchors.verticalCenter: parent.verticalCenter
                    visible: name === undefined && !currentMarker
                }

                Label {
                    text: qsTr("Current")
                    font.italic: true
                    anchors.verticalCenter: parent.verticalCenter
                    visible: currentMarker
                }
            }

            menu: ContextMenu {
                MenuItem {
                    id: menuSetBookmark
                    text: qsTr("Set bookmark")
                    onClicked: {
                        if (currentMarker)
                            user.subscribe(pageModel.cid(), false)
                        else
                            user.subscribeAt(pageModel.cid(), ord)
                    }
                }
            }

            onClicked: {
                pageModel.setCursor(ord)
                pageStack.pop()
            }
        }
    }
}
