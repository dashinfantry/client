/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: browsePage

    BusyIndicator {
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: user.loading
    }

    allowedOrientations: Orientation.All
    backNavigation: !panel.open

    Component.onCompleted: {
        browseModel.filterSubscribed = false;
        browseModel.sortType = 0;
        browseModel.setSearch("");
        pageModel.autoBookmark = false;
        pageModel.autoSwitch = false;
    }

    SilicaListView {
        id: browseList
        model: browseModel
        anchors.fill: parent
        currentIndex: -1

        VerticalScrollDecorator {
            flickable: browseList
        }

        PullDownMenu {
            MenuItem {
                id: browseOptionsItem
                text: qsTr("Options")
                onClicked: panel.open = true
            }
        }

        header: Column {
            width: parent.width

            PageHeader {
                id: header
                title: qsTr("Browse comics")
            }

            SearchField {
                width: parent.width
                onTextChanged: browseModel.setSearch(text)
            }
        }

        delegate: ListItem {
            id: delegate

            Image {
                source: "image://theme/icon-s-favorite"
                visible: subscribed
                anchors.verticalCenter: parent.verticalCenter
            }

            Label {
                x: Theme.horizontalPageMargin
                text: title
                anchors.verticalCenter: parent.verticalCenter
                color: delegate.highlighted ? Theme.highlightColor : Theme.primaryColor
            }

            menu: ContextMenu {
                MenuItem {
                    id: menuSubscribe
                    text: qsTr("Subscribe")
                    visible: !subscribed
                    onClicked: user.subscribe(cid, bookmarkFirst.checked)
                }

                MenuItem {
                    id: menuUnsubscribe
                    text: qsTr("Unsubscribe")
                    visible: subscribed
                    onClicked: {
                        var c = cid
                        Remorse.itemAction(delegate,
                                           qsTr("Unsubscribing"),
                                           function() { user.unsubscribe(c) })
                    }
                }

                MenuItem {
                    text: qsTr("View entry on Piperka")
                    onClicked: Qt.openUrlExternally("https://piperka.net/info.html?cid="+cid);
                }
            }

            onClicked: {
                pageModel.loadComic(cid);
                pageStack.push(Qt.resolvedUrl("ReaderPage.qml"))
            }
        }
    }

    DockedPanel {
        id: panel

        width: parent.width
        height: parent.height
        modal: true

        dock: Dock.Right

        Column {
            width: parent.width
            PageHeader {
                title: qsTr("Options")
            }

            ComboBox {
                id: sortType
                label: qsTr("Sort type")
                currentIndex: 0
                menu: ContextMenu {
                    MenuItem {
                        text: qsTr("Alphabetical")
                    }
                    MenuItem {
                        text: qsTr("Most popular")
                    }
                    MenuItem {
                        text: qsTr("Date added")
                    }
                    MenuItem {
                        text: qsTr("Most recently updated")
                    }
                    onClicked: {
                        browseModel.sortType = sortType.currentIndex
                    }
                }
            }

            TextSwitch {
                id: bookmarkFirst
                text: qsTr("Bookmark first page")
                anchors {
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
            }

            TextSwitch {
                id: showOnlySubscribed
                objectName: "browseSubscribed"
                text: qsTr("Show only subscribed comics")
                anchors {
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                onClicked: browseModel.filterSubscribed = checked
            }
        }
     }
}
