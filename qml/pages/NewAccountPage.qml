/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0
import net.piperka 0.1

Dialog {
    id: dialog
    allowedOrientations: Orientation.All

    Column {
        id: column
        width: parent.width

        DialogHeader {
            title: qsTr("Create account")
        }

        Label {
            x: Theme.horizontalPageMargin
            text: qsTr("Account name reserved. Please try again.")
            visible: user.storedCreatePassword != ""
        }

        TextField {
            id: nameField
            width: parent.width
            inputMethodHints: Qt.ImhNoAutoUppercase
            label: qsTr("User name")
            placeholderText: qsTr("User name")
            validator: RegExpValidator { regExp: /^.{2,}/ }
            EnterKey.enabled: text.length >= 2
            EnterKey.iconSource: "image://theme/icon-m-enter-next"
            EnterKey.onClicked: emailField.focus = true
        }

        TextField {
            id: emailField
            width: parent.width
            inputMethodHints: Qt.ImhNoAutoUppercase
            label: qsTr("email")
            placeholderText: qsTr("email (optional)")
            text: user.storedCreateEmail
            EnterKey.iconSource: "image://theme/icon-m-enter-next"
            EnterKey.onClicked: passwordField.focus = true
        }

        PasswordField {
            id: passwordField
            width: parent.width
            label: qsTr("Password")
            placeholderText: qsTr("Password")
            validator: RegExpValidator { regExp: /^.{4,}/ }
            objectName: "newAccountPassword"
            text: user.storedCreatePassword
            EnterKey.enabled: text.length >= 4
            EnterKey.iconSource: "image://theme/icon-m-enter-next"
            EnterKey.onClicked: passwordFieldAgain.focus = true
        }

        PasswordField {
            id: passwordFieldAgain
            width: parent.width
            label: qsTr("Retype password")
            placeholderText: qsTr("Retype password")
            validator: PasswordValidator { }
            text: user.storedCreatePassword
            EnterKey.enabled: text.length >= 4
            EnterKey.onClicked: dialog.accept()
        }

        TextSwitch {
            id: rememberField
            width: parent.width
            text: qsTr("Remember me")
            checked: user.rememberMe
        }
    }

    canAccept: nameField.acceptableInput
               && passwordField.acceptableInput
               && passwordFieldAgain.acceptableInput
    onAccepted: {
        user.createAccount(nameField.text, emailField.text,
                           passwordField.text, rememberField.checked)
    }
}
