/**************************************************************************
** Piperka Client
** Copyright (C) 2019  Kari Pahula
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License along
** with this program; if not, write to the Free Software Foundation, Inc.,
** 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
**************************************************************************/

import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: page
    allowedOrientations: Orientation.All

    BusyIndicator {
        id: busy
        size: BusyIndicatorSize.Large
        anchors.centerIn: parent
        running: user.loading || updatesModel.subscriptionFlag
    }

    Connections {
        target: user

        onLoginFailed: {
            pageStack.completeAnimation();
            pageStack.push(Qt.resolvedUrl("LoginPage.qml"))
        }

        onCreateAccountNameReserved: {
            pageStack.completeAnimation();
            pageStack.push(Qt.resolvedUrl("NewAccountPage.qml"));
        }

        onNetworkError: {
            pageStack.completeAnimation();
            pageStack.push(Qt.resolvedUrl("NetworkErrorPage.qml"));
        }

        onForceLogout: {
            pageStack.completeAnimation();
            pageStack.push(Qt.resolvedUrl("ForceLogout.qml"))
        }

        onSilentSyncFailureChanged: {
            syncFailedLabel.text = user.silentSyncFailure == 1 ?
                        qsTr("The last scheduled sync failed. The client will retry hourly.") :
                        user.silentSyncFailure > 1 ?
                            qsTr("Last %L1 scheduled syncs failed. The client will retry hourly.").arg(user.silentSyncFailure) :
                            "";
        }
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        PullDownMenu {
            MenuItem {
                id: newAccountItem
                text: qsTr("Create account")
                onClicked: {
                    user.resetStoredAccountDetails();
                    pageStack.push(Qt.resolvedUrl("NewAccountPage.qml"));
                }
                visible: !user.logged
            }

            MenuItem {
                id: loginMenuItem
                text: qsTr("Login")
                onClicked: {
                    user.resetStoredAccountDetails();
                    pageStack.push(Qt.resolvedUrl("LoginPage.qml"))
                }
                visible: !user.logged
            }

            MenuItem {
                id: logoutMenuItem
                text: qsTr("Logout")
                onClicked: Remorse.popupAction(page, qsTr("Logging out"), function() {user.logout();})
                visible: user.logged
            }

            MenuItem {
                text: qsTr("Synchronize now")
                onClicked: user.syncNow(true)
            }
        }

        Column {
            id: column

            width: page.width;

            PageHeader {
                id: header
                title: "Piperka"
                Connections {
                    target: user
                    onLoggedChange: {
                        if (user.name) {
                            header.title = user.name + " — Piperka"
                        } else {
                            header.title = "Piperka"
                        }
                    }
                }
            }

            BackgroundItem {
                Label {
                    enabled: !user.loading
                    x: Theme.horizontalPageMargin
                    text: qsTr("Browse comics")
                    color: Theme.primaryColor
                    font.pixelSize: Theme.fontSizeExtraLarge
                }
                onClicked: pageStack.push(Qt.resolvedUrl("BrowsePage.qml"))
            }

            BackgroundItem {
                enabled: !updatesModel.noUnread && !user.loading
                Label {
                    x: Theme.horizontalPageMargin
                    text: qsTr("Updates")+
                          (!updatesModel.noUnread ?
                               (" ("+updatesModel.unreadPages+" / "+updatesModel.rowCount()+")") : "")
                    color: !updatesModel.noUnread ? Theme.primaryColor : Theme.secondaryColor
                    font.pixelSize: Theme.fontSizeExtraLarge
                }
                onClicked: pageStack.push(Qt.resolvedUrl("UpdatesPage.qml"))
            }

            BackgroundItem {
                enabled: !updatesModel.noUnread && !user.loading

                Label {
                    x: Theme.horizontalPageMargin
                    text: qsTr("Quick read")
                    color: !updatesModel.noUnread ? Theme.primaryColor : Theme.secondaryColor
                    font.pixelSize: Theme.fontSizeExtraLarge
                }
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("UpdatesPage.qml"), {}, PageStackAction.Immediate)
                    pageModel.loadComic(updatesModel.firstCid());
                    pageModel.autoBookmark = true;
                    pageModel.autoSwitch = true;
                    pageStack.push(Qt.resolvedUrl("ReaderPage.qml"));
                }
            }

            Label {
                x: Theme.horizontalPageMargin
                width: parent.width
                wrapMode: Text.WordWrap
                color: Theme.highlightColor
                text: user.noSubscriptions ?
                          qsTr("Select comics to read from the browse comics page.") :
                          qsTr("You have no unread comics. Wait for updates or subscribe to more comics.")
                visible: updatesModel.noUnread
                         && !updatesModel.subscriptionFlag
                         && !user.loading
            }
            Label {
                id: syncFailedLabel
                x: Theme.horizontalPageMargin
                width: parent.width-2*Theme.horizontalPageMargin
                wrapMode: Text.WordWrap
                color: Theme.highlightColor
                text: ""
                visible: user.silentSyncFailure > 0
            }
        }
    }
}
