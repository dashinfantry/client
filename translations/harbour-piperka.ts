<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AllReadPage</name>
    <message>
        <source>All caught up!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BrowsePage</name>
    <message>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Browse comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alphabetical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Most popular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Date added</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Most recently updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bookmark first page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show only subscribed comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subscribe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unsubscribe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unsubscribing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View entry on Piperka</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>All caught up!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ForceLogout</name>
    <message>
        <source>The server didn&apos;t recognize your user session. You have been logged out. Please try logging in again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remember me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import bookmarks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Importing bookmarks will overwrite the ones on server. Not importing will discard local bookmarks.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <source>Create account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Logging out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Synchronize now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Browse comics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quick read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have no unread comics. Wait for updates or subscribe to more comics.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select comics to read from the browse comics page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Last %L1 scheduled syncs failed. The client will retry hourly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The last scheduled sync failed. The client will retry hourly.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NetworkErrorPage</name>
    <message>
        <source>Detail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Network failure</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewAccountPage</name>
    <message>
        <source>Create account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>User name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>email (optional)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>email</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Retype password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account name reserved. Please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remember me</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PageDetailPage</name>
    <message>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Jump to...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Currently viewing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subscribed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set bookmark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Subscription controls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move bookmark on navigation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Newest page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use the following link to open in external browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdatesPage</name>
    <message>
        <source>Synchronize now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updates</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Least new pages first</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Most recently updated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Alphabetical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Offset back by one</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
